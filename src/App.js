import './App.css';
import React, { useState } from 'react';

function App() {
  const [formData, setFormData] = useState({name: '',age: ''});
  const [displayData, setDisplayData] = useState('');

  const handleChange = (props) => {
    const {name, value} = props.target;
    setFormData({
      ...formData,
      [name]: name === 'name' ? value.charAt(0).toUpperCase() + value.slice(1) : value
    });
  };

  const handleSubmit = (props) => {
    props.preventDefault();
    const {name, age} = formData;
    if (name && Number.isInteger(Number(age)) && Number(age) >= 0 && Number(age) <= 125) {
      const person = { name, age: parseInt(age) };
      setDisplayData(JSON.stringify(person));
    } 
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label>
          Name:
          <input type="text" name="name" value={formData.name} onChange={handleChange} />
        </label>
        <br />
        <label>
          Age:
          <input type="text" name="age" value={formData.age} onChange={handleChange} />
        </label>
        <br />
        <button type="submit">Submit</button>
      </form>
      {displayData && <p>{displayData}</p>}
    </div>
  );
}

export default App;